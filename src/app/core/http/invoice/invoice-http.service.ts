import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AUTHENTICATION_REQUIRED, AUTHENTICATION_REQUIRED_OPTIONS } from '@kominal/core-user-service-angular-client';
import { PaginationRequest, PaginationResponse, toPaginationParams } from '@kominal/lib-angular-pagination';
import { Invoice } from '../../models/invoice';

@Injectable({
	providedIn: 'root',
})
export class InvoiceHttpService {
	constructor(private httpClient: HttpClient) {}

	list(tenantId: string, paginationRequest: PaginationRequest): Promise<PaginationResponse<Invoice>> {
		return this.httpClient
			.get<PaginationResponse<Invoice>>(`/controller/${tenantId}/invoices`, {
				params: toPaginationParams(paginationRequest),
				headers: AUTHENTICATION_REQUIRED,
			})
			.toPromise();
	}

	get(tenantId: string, invoiceId: string): Promise<Invoice> {
		return this.httpClient.get<Invoice>(`/controller/${tenantId}/invoices/${invoiceId}`, AUTHENTICATION_REQUIRED_OPTIONS).toPromise();
	}

	create(tenantId: string, invoice: Invoice): Promise<{ _id: string }> {
		return this.httpClient.post<{ _id: string }>(`/controller/${tenantId}/invoices`, invoice, AUTHENTICATION_REQUIRED_OPTIONS).toPromise();
	}

	update(tenantId: string, invoice: Invoice): Promise<void> {
		return this.httpClient
			.put<void>(`/controller/${tenantId}/invoices/${invoice._id}`, invoice, AUTHENTICATION_REQUIRED_OPTIONS)
			.toPromise();
	}

	delete(tenantId: string, invoiceId: string): Promise<void> {
		return this.httpClient.delete<void>(`/controller/${tenantId}/invoices/${invoiceId}`, AUTHENTICATION_REQUIRED_OPTIONS).toPromise();
	}
}
