import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AUTHENTICATION_REQUIRED_OPTIONS } from '@kominal/core-user-service-angular-client';
import { Dashboard } from '../../models/dashboard';

@Injectable({
	providedIn: 'root',
})
export class DashboardHttpService {
	constructor(private httpClient: HttpClient) {}

	get(tenantId: string): Promise<Dashboard> {
		return this.httpClient.get<Dashboard>(`/controller/${tenantId}/dashboard`, AUTHENTICATION_REQUIRED_OPTIONS).toPromise();
	}
}
