import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AUTHENTICATION_REQUIRED, AUTHENTICATION_REQUIRED_OPTIONS } from '@kominal/core-user-service-angular-client';
import { PaginationRequest, PaginationResponse, toPaginationParams } from '@kominal/lib-angular-pagination';
import { Transaction } from '../../models/transaction';

@Injectable({
	providedIn: 'root',
})
export class TransactionHttpService {
	constructor(private httpClient: HttpClient) {}

	list(tenantId: string, paginationRequest: PaginationRequest): Promise<PaginationResponse<Transaction>> {
		return this.httpClient
			.get<PaginationResponse<Transaction>>(`/controller/${tenantId}/transactions`, {
				params: toPaginationParams(paginationRequest),
				headers: AUTHENTICATION_REQUIRED,
			})
			.toPromise();
	}

	get(tenantId: string, transactionId: string): Promise<Transaction> {
		return this.httpClient
			.get<Transaction>(`/controller/${tenantId}/transactions/${transactionId}`, AUTHENTICATION_REQUIRED_OPTIONS)
			.toPromise();
	}

	create(tenantId: string, transaction: Transaction): Promise<{ _id: string }> {
		return this.httpClient
			.post<{ _id: string }>(`/controller/${tenantId}/transactions`, transaction, AUTHENTICATION_REQUIRED_OPTIONS)
			.toPromise();
	}

	update(tenantId: string, transaction: Transaction): Promise<void> {
		return this.httpClient
			.put<void>(`/controller/${tenantId}/transactions/${transaction._id}`, transaction, AUTHENTICATION_REQUIRED_OPTIONS)
			.toPromise();
	}

	delete(tenantId: string, transactionId: string): Promise<void> {
		return this.httpClient
			.delete<void>(`/controller/${tenantId}/transactions/${transactionId}`, AUTHENTICATION_REQUIRED_OPTIONS)
			.toPromise();
	}
}
