import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AUTHENTICATION_REQUIRED_OPTIONS } from '@kominal/core-user-service-angular-client';
import { Tenant } from '../../models/tenant';

@Injectable({
	providedIn: 'root',
})
export class TenantHttpService {
	constructor(private httpClient: HttpClient) {}

	list(): Promise<Tenant[]> {
		return this.httpClient.get<Tenant[]>(`/controller/tenants`, AUTHENTICATION_REQUIRED_OPTIONS).toPromise();
	}

	get(tenantId: string): Promise<Tenant> {
		return this.httpClient.get<Tenant>(`/controller/tenants/${tenantId}`, AUTHENTICATION_REQUIRED_OPTIONS).toPromise();
	}

	create(tenant: Tenant): Promise<{ _id: string }> {
		return this.httpClient.post<{ _id: string }>(`/controller/tenants`, tenant, AUTHENTICATION_REQUIRED_OPTIONS).toPromise();
	}

	update(tenant: Tenant): Promise<void> {
		return this.httpClient.put<void>(`/controller/tenants/${tenant._id}`, tenant, AUTHENTICATION_REQUIRED_OPTIONS).toPromise();
	}

	delete(tenantId: string): Promise<void> {
		return this.httpClient.delete<void>(`/controller/tenants/${tenantId}`, AUTHENTICATION_REQUIRED_OPTIONS).toPromise();
	}
}
