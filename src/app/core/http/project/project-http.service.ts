import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AUTHENTICATION_REQUIRED, AUTHENTICATION_REQUIRED_OPTIONS } from '@kominal/core-user-service-angular-client';
import { PaginationRequest, PaginationResponse, toPaginationParams } from '@kominal/lib-angular-pagination';
import { Project } from '../../models/project';

@Injectable({
	providedIn: 'root',
})
export class ProjectHttpService {
	constructor(private httpClient: HttpClient) {}

	list(tenantId: string, paginationRequest: PaginationRequest): Promise<PaginationResponse<Project>> {
		return this.httpClient
			.get<PaginationResponse<Project>>(`/controller/${tenantId}/projects`, {
				params: toPaginationParams(paginationRequest),
				headers: AUTHENTICATION_REQUIRED,
			})
			.toPromise();
	}

	get(tenantId: string, projectId: string): Promise<Project> {
		return this.httpClient.get<Project>(`/controller/${tenantId}/projects/${projectId}`, AUTHENTICATION_REQUIRED_OPTIONS).toPromise();
	}

	create(tenantId: string, project: Project): Promise<{ _id: string }> {
		return this.httpClient.post<{ _id: string }>(`/controller/${tenantId}/projects`, project, AUTHENTICATION_REQUIRED_OPTIONS).toPromise();
	}

	update(tenantId: string, project: Project): Promise<void> {
		return this.httpClient
			.put<void>(`/controller/${tenantId}/projects/${project._id}`, project, AUTHENTICATION_REQUIRED_OPTIONS)
			.toPromise();
	}

	delete(tenantId: string, projectId: string): Promise<void> {
		return this.httpClient.delete<void>(`/controller/${tenantId}/projects/${projectId}`, AUTHENTICATION_REQUIRED_OPTIONS).toPromise();
	}
}
