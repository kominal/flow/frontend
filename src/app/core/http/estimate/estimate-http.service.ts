import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AUTHENTICATION_REQUIRED, AUTHENTICATION_REQUIRED_OPTIONS } from '@kominal/core-user-service-angular-client';
import { PaginationRequest, PaginationResponse, toPaginationParams } from '@kominal/lib-angular-pagination';
import { Estimate } from '../../models/estimate';

@Injectable({
	providedIn: 'root',
})
export class EstimateHttpService {
	constructor(private httpClient: HttpClient) {}

	list(tenantId: string, paginationRequest: PaginationRequest): Promise<PaginationResponse<Estimate>> {
		return this.httpClient
			.get<PaginationResponse<Estimate>>(`/controller/${tenantId}/estimates`, {
				params: toPaginationParams(paginationRequest),
				headers: AUTHENTICATION_REQUIRED,
			})
			.toPromise();
	}

	get(tenantId: string, estimateId: string): Promise<Estimate> {
		return this.httpClient.get<Estimate>(`/controller/${tenantId}/estimates/${estimateId}`, AUTHENTICATION_REQUIRED_OPTIONS).toPromise();
	}

	create(tenantId: string, estimate: Estimate): Promise<{ _id: string }> {
		return this.httpClient
			.post<{ _id: string }>(`/controller/${tenantId}/estimates`, estimate, AUTHENTICATION_REQUIRED_OPTIONS)
			.toPromise();
	}

	update(tenantId: string, estimate: Estimate): Promise<void> {
		return this.httpClient
			.put<void>(`/controller/${tenantId}/estimates/${estimate._id}`, estimate, AUTHENTICATION_REQUIRED_OPTIONS)
			.toPromise();
	}

	delete(tenantId: string, estimateId: string): Promise<void> {
		return this.httpClient.delete<void>(`/controller/${tenantId}/estimates/${estimateId}`, AUTHENTICATION_REQUIRED_OPTIONS).toPromise();
	}
}
