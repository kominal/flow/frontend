import { TestBed } from '@angular/core/testing';

import { EstimateHttpService } from './estimate-http.service';

describe('EstimateHttpService', () => {
  let service: EstimateHttpService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(EstimateHttpService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
