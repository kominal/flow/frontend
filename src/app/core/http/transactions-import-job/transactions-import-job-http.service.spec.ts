import { TestBed } from '@angular/core/testing';

import { TransactionsImportJobHttpService } from './transactions-import-job-http.service';

describe('TransactionsImportJobHttpService', () => {
  let service: TransactionsImportJobHttpService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(TransactionsImportJobHttpService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
