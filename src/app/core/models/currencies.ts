import { BehaviorSubject } from 'rxjs';

export const currenciesSubject = new BehaviorSubject<string[]>(['EUR']);
