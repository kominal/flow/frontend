export interface Estimate {
	_id?: string;
	tenantId: string;
	invoiceIds: string[];
}
