export interface Invoice {
	_id?: string;
	tenantId: string;
	participantId: string[];
	paymentIds: string[];
	estimateIds: string[];
	projectIds: string[];
}
