import { Injectable } from '@angular/core';
import { DialogService } from '@kominal/lib-angular-dialog';
import { LogService } from '@kominal/lib-angular-logging';
import { EMPTY_PAGINATION_RESPONSE, PaginationRequest, PaginationResponse } from '@kominal/lib-angular-pagination';
import { TransactionsImportJobHttpService } from '../../http/transactions-import-job/transactions-import-job-http.service';
import { TransactionsImportJob } from '../../models/transactionsImportJob';
import { TenantService } from '../tenant/tenant.service';

@Injectable({
	providedIn: 'root',
})
export class TransactionsImportJobService {
	constructor(
		private transactionsImportJobHttpService: TransactionsImportJobHttpService,
		private tenantService: TenantService,
		private dialogService: DialogService,
		private logService: LogService
	) {}

	async list(paginationRequest: PaginationRequest): Promise<PaginationResponse<TransactionsImportJob>> {
		try {
			return await this.transactionsImportJobHttpService.list(this.tenantService.getCurrentTenantId(), paginationRequest);
		} catch (e) {
			this.logService.handleError(e);
		}
		return EMPTY_PAGINATION_RESPONSE;
	}

	async get(transactionsImportJobId: string): Promise<TransactionsImportJob | undefined> {
		try {
			return await this.transactionsImportJobHttpService.get(this.tenantService.getCurrentTenantId(), transactionsImportJobId);
		} catch (e) {
			this.logService.handleError(e);
		}
	}

	create(transactionsImportJob: TransactionsImportJob, files: File[]): Promise<{ _id: string }> {
		return this.transactionsImportJobHttpService.create(this.tenantService.getCurrentTenantId(), transactionsImportJob, files);
	}

	update(transactionsImportJob: TransactionsImportJob): Promise<void> {
		return this.transactionsImportJobHttpService.update(this.tenantService.getCurrentTenantId(), transactionsImportJob);
	}

	async delete(transactionsImportJob: TransactionsImportJob): Promise<boolean> {
		try {
			if (transactionsImportJob._id && (await this.dialogService.openConfirmDeleteDialog(transactionsImportJob._id))) {
				await this.transactionsImportJobHttpService.delete(this.tenantService.getCurrentTenantId(), transactionsImportJob._id);
				return true;
			}
		} catch (e) {
			this.logService.handleError(e);
		}
		return false;
	}
}
