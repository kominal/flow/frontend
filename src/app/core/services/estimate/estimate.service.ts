import { Injectable } from '@angular/core';
import { DialogService } from '@kominal/lib-angular-dialog';
import { LogService } from '@kominal/lib-angular-logging';
import { EMPTY_PAGINATION_RESPONSE, PaginationRequest, PaginationResponse } from '@kominal/lib-angular-pagination';
import { EstimateHttpService } from '../../http/estimate/estimate-http.service';
import { Estimate } from '../../models/estimate';
import { TenantService } from '../tenant/tenant.service';

@Injectable({
	providedIn: 'root',
})
export class EstimateService {
	constructor(
		private estimateHttpService: EstimateHttpService,
		private tenantService: TenantService,
		private dialogService: DialogService,
		private logService: LogService
	) {}

	async list(paginationRequest: PaginationRequest): Promise<PaginationResponse<Estimate>> {
		try {
			return await this.estimateHttpService.list(this.tenantService.getCurrentTenantId(), paginationRequest);
		} catch (e) {
			this.logService.handleError(e);
		}
		return EMPTY_PAGINATION_RESPONSE;
	}

	async get(accountId: string): Promise<Estimate | undefined> {
		try {
			return await this.estimateHttpService.get(this.tenantService.getCurrentTenantId(), accountId);
		} catch (e) {
			this.logService.handleError(e);
		}
	}

	create(estimate: Estimate): Promise<{ _id: string }> {
		return this.estimateHttpService.create(this.tenantService.getCurrentTenantId(), estimate);
	}

	update(estimate: Estimate): Promise<void> {
		return this.estimateHttpService.update(this.tenantService.getCurrentTenantId(), estimate);
	}

	createOrUpdate(estimate: Estimate): Promise<void | { _id: string }> {
		return estimate._id ? this.update(estimate) : this.create(estimate);
	}

	async delete(estimate: Estimate): Promise<boolean> {
		try {
			if (estimate._id && (await this.dialogService.openConfirmDeleteDialog(estimate._id))) {
				await this.estimateHttpService.delete(this.tenantService.getCurrentTenantId(), estimate._id);
				return true;
			}
		} catch (e) {
			this.logService.handleError(e);
		}
		return false;
	}
}
