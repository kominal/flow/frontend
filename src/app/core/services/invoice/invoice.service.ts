import { Injectable } from '@angular/core';
import { DialogService } from '@kominal/lib-angular-dialog';
import { LogService } from '@kominal/lib-angular-logging';
import { EMPTY_PAGINATION_RESPONSE, PaginationRequest, PaginationResponse } from '@kominal/lib-angular-pagination';
import { InvoiceHttpService } from '../../http/invoice/invoice-http.service';
import { Invoice } from '../../models/invoice';
import { TenantService } from '../tenant/tenant.service';

@Injectable({
	providedIn: 'root',
})
export class InvoiceService {
	constructor(
		private invoiceHttpService: InvoiceHttpService,
		private tenantService: TenantService,
		private dialogService: DialogService,
		private logService: LogService
	) {}

	async list(paginationRequest: PaginationRequest): Promise<PaginationResponse<Invoice>> {
		try {
			return await this.invoiceHttpService.list(this.tenantService.getCurrentTenantId(), paginationRequest);
		} catch (e) {
			this.logService.handleError(e);
		}
		return EMPTY_PAGINATION_RESPONSE;
	}

	async get(invoiceId: string): Promise<Invoice | undefined> {
		try {
			return await this.invoiceHttpService.get(this.tenantService.getCurrentTenantId(), invoiceId);
		} catch (e) {
			this.logService.handleError(e);
		}
	}

	create(invoice: Invoice): Promise<{ _id: string }> {
		return this.invoiceHttpService.create(this.tenantService.getCurrentTenantId(), invoice);
	}

	update(invoice: Invoice): Promise<void> {
		return this.invoiceHttpService.update(this.tenantService.getCurrentTenantId(), invoice);
	}

	createOrUpdate(invoice: Invoice): Promise<void | { _id: string }> {
		return invoice._id ? this.update(invoice) : this.create(invoice);
	}

	async delete(invoice: Invoice): Promise<boolean> {
		try {
			if (invoice._id && (await this.dialogService.openConfirmDeleteDialog(invoice._id))) {
				await this.invoiceHttpService.delete(this.tenantService.getCurrentTenantId(), invoice._id);
				return true;
			}
		} catch (e) {
			this.logService.handleError(e);
		}
		return false;
	}
}
