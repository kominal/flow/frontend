import { Injectable } from '@angular/core';
import { DialogService } from '@kominal/lib-angular-dialog';
import { LogService } from '@kominal/lib-angular-logging';
import { EMPTY_PAGINATION_RESPONSE, PaginationRequest, PaginationResponse } from '@kominal/lib-angular-pagination';
import { ProjectHttpService } from '../../http/project/project-http.service';
import { Project } from '../../models/project';
import { TenantService } from '../tenant/tenant.service';

@Injectable({
	providedIn: 'root',
})
export class ProjectService {
	constructor(
		private projectHttpService: ProjectHttpService,
		private tenantService: TenantService,
		private dialogService: DialogService,
		private logService: LogService
	) {}

	async list(paginationRequest: PaginationRequest): Promise<PaginationResponse<Project>> {
		try {
			return await this.projectHttpService.list(this.tenantService.getCurrentTenantId(), paginationRequest);
		} catch (e) {
			this.logService.handleError(e);
		}
		return EMPTY_PAGINATION_RESPONSE;
	}

	async get(projectId: string): Promise<Project | undefined> {
		try {
			return await this.projectHttpService.get(this.tenantService.getCurrentTenantId(), projectId);
		} catch (e) {
			this.logService.handleError(e);
		}
	}

	create(project: Project): Promise<{ _id: string }> {
		return this.projectHttpService.create(this.tenantService.getCurrentTenantId(), project);
	}

	update(project: Project): Promise<void> {
		return this.projectHttpService.update(this.tenantService.getCurrentTenantId(), project);
	}

	createOrUpdate(project: Project): Promise<void | { _id: string }> {
		return project._id ? this.update(project) : this.create(project);
	}

	async delete(project: Project): Promise<boolean> {
		try {
			if (project._id && (await this.dialogService.openConfirmDeleteDialog(project._id))) {
				await this.projectHttpService.delete(this.tenantService.getCurrentTenantId(), project._id);
				return true;
			}
		} catch (e) {
			this.logService.handleError(e);
		}
		return false;
	}
}
