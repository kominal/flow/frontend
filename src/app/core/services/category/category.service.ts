import { Injectable } from '@angular/core';
import { DialogService } from '@kominal/lib-angular-dialog';
import { LogService } from '@kominal/lib-angular-logging';
import { EMPTY_PAGINATION_RESPONSE, PaginationRequest, PaginationResponse } from '@kominal/lib-angular-pagination';
import { CategoryHttpService } from '../../http/category/category-http.service';
import { Category } from '../../models/category';
import { TenantService } from '../tenant/tenant.service';

@Injectable({
	providedIn: 'root',
})
export class CategoryService {
	constructor(
		private categoryHttpService: CategoryHttpService,
		private tenantService: TenantService,
		private dialogService: DialogService,
		private logService: LogService
	) {}

	async list(paginationRequest: PaginationRequest): Promise<PaginationResponse<Category>> {
		try {
			return await this.categoryHttpService.list(this.tenantService.getCurrentTenantId(), paginationRequest);
		} catch (e) {
			this.logService.handleError(e);
		}
		return EMPTY_PAGINATION_RESPONSE;
	}

	async get(categoryId: string): Promise<Category | undefined> {
		try {
			return await this.categoryHttpService.get(this.tenantService.getCurrentTenantId(), categoryId);
		} catch (e) {
			this.logService.handleError(e);
		}
	}

	create(category: Category): Promise<{ _id: string }> {
		return this.categoryHttpService.create(this.tenantService.getCurrentTenantId(), category);
	}

	update(category: Category): Promise<void> {
		return this.categoryHttpService.update(this.tenantService.getCurrentTenantId(), category);
	}

	createOrUpdate(category: Category): Promise<void | { _id: string }> {
		return category._id ? this.update(category) : this.create(category);
	}

	async delete(category: Category): Promise<boolean> {
		try {
			if (category._id && (await this.dialogService.openConfirmDeleteDialog(category._id))) {
				await this.categoryHttpService.delete(this.tenantService.getCurrentTenantId(), category._id);
				return true;
			}
		} catch (e) {
			this.logService.handleError(e);
		}
		return false;
	}
}
