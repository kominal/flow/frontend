import { Component } from '@angular/core';
import { UserService } from '@kominal/core-user-service-angular-client';

@Component({
	selector: 'app-root',
	templateUrl: './app.component.html',
	styleUrls: ['./app.component.scss'],
})
export class AppComponent {
	constructor(userService: UserService) {
		if (userService.isLoggedIn()) {
			userService.getJWT();
		}
	}
}
