import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthenticationComponent, UserGuard } from '@kominal/core-user-service-angular-client';
import { AccountsComponent } from './modules/accounts/accounts.component';
import { CategoriesComponent } from './modules/categories/categories.component';
import { DashboardComponent } from './modules/dashboard/dashboard.component';
import { HomeComponent } from './modules/home/home.component';
import { InvoicesComponent } from './modules/invoices/invoices.component';
import { ParticipantsComponent } from './modules/participants/participants.component';
import { ProjectsComponent } from './modules/projects/projects.component';
import { ReportingComponent } from './modules/reporting/reporting.component';
import { SettingsComponent } from './modules/settings/settings.component';
import { TaxationComponent } from './modules/taxation/taxation.component';
import { TransactionsComponent } from './modules/transactions/transactions.component';
import { UsersComponent } from './modules/users/users.component';

const routes: Routes = [
	{
		path: 'authentication',
		component: AuthenticationComponent,
		loadChildren: () => import('@kominal/core-user-service-angular-client').then((m) => m.AuthenticationModule),
	},
	{
		path: 'dashboard',
		component: DashboardComponent,
		loadChildren: () => import('./modules/dashboard/dashboard.module').then((m) => m.DashboardModule),
		canActivate: [UserGuard],
	},
	{
		path: 'tenant/:tenantId/dashboard',
		component: DashboardComponent,
		loadChildren: () => import('./modules/dashboard/dashboard.module').then((m) => m.DashboardModule),
		canActivate: [UserGuard],
	},
	{
		path: 'tenant/:tenantId/transactions',
		component: TransactionsComponent,
		loadChildren: () => import('./modules/transactions/transactions.module').then((m) => m.TransactionsModule),
		canActivate: [UserGuard],
	},
	{
		path: 'tenant/:tenantId/invoices',
		component: InvoicesComponent,
		loadChildren: () => import('./modules/invoices/invoices.module').then((m) => m.InvoicesModule),
		canActivate: [UserGuard],
	},
	{
		path: 'tenant/:tenantId/estimates',
		component: InvoicesComponent,
		loadChildren: () => import('./modules/estimates/estimates.module').then((m) => m.EstimatesModule),
		canActivate: [UserGuard],
	},
	{
		path: 'tenant/:tenantId/participants',
		component: ParticipantsComponent,
		loadChildren: () => import('./modules/participants/participants.module').then((m) => m.ParticipantsModule),
		canActivate: [UserGuard],
	},
	{
		path: 'tenant/:tenantId/projects',
		component: ProjectsComponent,
		loadChildren: () => import('./modules/projects/projects.module').then((m) => m.ProjectsModule),
		canActivate: [UserGuard],
	},
	{
		path: 'tenant/:tenantId/accounts',
		component: AccountsComponent,
		loadChildren: () => import('./modules/accounts/accounts.module').then((m) => m.AccountsModule),
		canActivate: [UserGuard],
	},
	{
		path: 'tenant/:tenantId/categories',
		component: CategoriesComponent,
		loadChildren: () => import('./modules/categories/categories.module').then((m) => m.CategoriesModule),
		canActivate: [UserGuard],
	},
	{
		path: 'tenant/:tenantId/reporting',
		component: ReportingComponent,
		loadChildren: () => import('./modules/reporting/reporting.module').then((m) => m.ReportingModule),
		canActivate: [UserGuard],
	},
	{
		path: 'tenant/:tenantId/taxation',
		component: TaxationComponent,
		loadChildren: () => import('./modules/taxation/taxation.module').then((m) => m.TaxationModule),
		canActivate: [UserGuard],
	},
	{
		path: 'tenant/:tenantId/settings',
		component: SettingsComponent,
		loadChildren: () => import('./modules/settings/settings.module').then((m) => m.SettingsModule),
		canActivate: [UserGuard],
		data: {
			permissions: ['flow.controller.tenant.settings'],
		},
	},
	{
		path: 'tenant/:tenantId/users',
		component: UsersComponent,
		loadChildren: () => import('./modules/users/users.module').then((m) => m.UsersModule),
		canActivate: [UserGuard],
		data: {
			permissions: ['core.user-service.write', 'core.user-service.read'],
		},
	},
	{
		path: '',
		component: HomeComponent,
		loadChildren: () => import('./modules/home/home.module').then((m) => m.HomeModule),
	},
	{ path: '**', redirectTo: '' },
];

@NgModule({
	imports: [RouterModule.forRoot(routes)],
	exports: [RouterModule],
})
export class AppRoutingModule {}
