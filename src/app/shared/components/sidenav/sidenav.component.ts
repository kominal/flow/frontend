import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { MatSidenav } from '@angular/material/sidenav';
import { Router } from '@angular/router';
import { UserService } from '@kominal/core-user-service-angular-client';
import { LayoutService } from '@kominal/lib-angular-layout';
import { ActionsService } from 'src/app/core/services/actions/actions.service';
import { TenantService } from 'src/app/core/services/tenant/tenant.service';

@Component({
	selector: 'app-sidenav',
	templateUrl: './sidenav.component.html',
	styleUrls: ['./sidenav.component.scss'],
})
export class SidenavComponent implements OnInit, OnDestroy {
	@ViewChild('sidenav', { static: true }) public sidenav!: MatSidenav;

	constructor(
		public userService: UserService,
		public layoutService: LayoutService,
		public tenantService: TenantService,
		public actionsService: ActionsService,
		public router: Router
	) {}

	ngOnInit(): void {
		this.actionsService.sidenav = this.sidenav;
	}

	ngOnDestroy(): void {
		this.actionsService.sidenav = undefined;
	}
}
