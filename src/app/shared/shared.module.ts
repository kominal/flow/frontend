import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { CoreUserServiceModule } from '@kominal/core-user-service-angular-client';
import { ClipboardModule } from '@kominal/lib-angular-clipboard';
import { FormModule } from '@kominal/lib-angular-form';
import { TableModule } from '@kominal/lib-angular-table';
import { TranslateModule } from '@ngx-translate/core';
import { SidenavComponent } from './components/sidenav/sidenav.component';
import { MaterialModule } from './material.module';

@NgModule({
	declarations: [SidenavComponent],
	imports: [
		CommonModule,
		TranslateModule,
		MaterialModule,
		FormsModule,
		ReactiveFormsModule,
		RouterModule,
		ClipboardModule,
		CoreUserServiceModule,
	],
	exports: [TranslateModule, MaterialModule, FormsModule, ReactiveFormsModule, SidenavComponent, TableModule, ClipboardModule, FormModule],
})
export class SharedModule {}
