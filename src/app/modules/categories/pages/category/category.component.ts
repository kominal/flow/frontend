import { Component } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { LogService } from '@kominal/lib-angular-logging';
import { Category } from 'src/app/core/models/category';
import { CategoryService } from 'src/app/core/services/category/category.service';
import { TenantService } from 'src/app/core/services/tenant/tenant.service';

@Component({
	selector: 'app-category',
	templateUrl: './category.component.html',
	styleUrls: ['./category.component.scss'],
})
export class CategoryComponent {
	public Object = Object;

	public loading = true;
	public form: FormGroup;

	constructor(
		public tenantService: TenantService,
		private categoryService: CategoryService,
		private logService: LogService,
		activatedRoute: ActivatedRoute
	) {
		this.form = this.createFormGroup();
		activatedRoute.params.subscribe(async (params) => {
			if (params.categoryId) {
				const category = await categoryService.get(params.categoryId);
				if (category) {
					this.form = this.createFormGroup(category);
				} else {
					this.navigateToParent();
				}
			}
			this.loading = false;
		});
	}

	createFormGroup(category?: Category): FormGroup {
		return new FormGroup({
			_id: new FormControl(category?._id),
			name: new FormControl(category?.name, Validators.required),
		});
	}

	async onSubmit() {
		this.loading = true;
		try {
			await this.categoryService.createOrUpdate(this.form.value);
			this.navigateToParent();
		} catch (e) {
			this.logService.handleError(e);
			this.loading = false;
		}
	}

	navigateToParent() {
		this.tenantService.navigateToTenant(['categories']);
	}
}
