import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MaterialModule } from 'src/app/shared/material.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { EditableComponent } from './components/editable/editable.component';
import { InvoiceViewComponent } from './components/invoice-view/invoice-view.component';
import { InvoicesRoutingModule } from './invoices-routing.module';
import { InvoicesComponent } from './invoices.component';
import { HomeComponent } from './pages/home/home.component';
import { InvoicesListComponent } from './pages/invoices-list/invoices-list.component';

@NgModule({
	declarations: [InvoicesComponent, InvoiceViewComponent, EditableComponent, InvoicesListComponent, HomeComponent],
	imports: [CommonModule, InvoicesRoutingModule, SharedModule, MaterialModule],
})
export class InvoicesModule {}
