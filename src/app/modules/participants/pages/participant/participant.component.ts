import { Component } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { LogService } from '@kominal/lib-angular-logging';
import { Participant } from 'src/app/core/models/participant';
import { ParticipantService } from 'src/app/core/services/participant/participant.service';
import { TenantService } from 'src/app/core/services/tenant/tenant.service';

@Component({
	selector: 'app-participant',
	templateUrl: './participant.component.html',
	styleUrls: ['./participant.component.scss'],
})
export class ParticipantComponent {
	public loading = true;
	public form: FormGroup;

	constructor(
		public tenantService: TenantService,
		private participantService: ParticipantService,
		private logService: LogService,
		activatedRoute: ActivatedRoute
	) {
		this.form = this.createFormGroup();

		activatedRoute.params.subscribe(async (params) => {
			if (params.participantId) {
				const participant = await participantService.get(params.participantId);
				if (participant) {
					this.form = this.createFormGroup(participant);
				} else {
					this.navigateToParent();
				}
			}
			this.loading = false;
		});
	}

	createFormGroup(participant?: Participant): FormGroup {
		return new FormGroup({
			_id: new FormControl(participant?._id),
			name: new FormControl(participant?.name, Validators.required),
		});
	}

	async onSubmit() {
		this.loading = true;
		try {
			await this.participantService.createOrUpdate(this.form.value);
			this.navigateToParent();
		} catch (e) {
			this.logService.handleError(e);
			this.loading = false;
		}
	}

	navigateToParent() {
		this.tenantService.navigateToTenant(['participants']);
	}
}
