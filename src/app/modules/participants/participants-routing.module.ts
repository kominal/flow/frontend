import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './pages/home/home.component';
import { ParticipantComponent } from './pages/participant/participant.component';

const routes: Routes = [
	{ path: '', component: HomeComponent },
	{ path: 'create', component: ParticipantComponent },
	{ path: ':participantId', component: ParticipantComponent },
	{ path: '**', redirectTo: '' },
];

@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule],
})
export class ParticipantsRoutingModule {}
