import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AdvancedComponent } from './pages/advanced/advanced.component';
import { MasterDataComponent } from './pages/master-data/master-data.component';

const routes: Routes = [
	{ path: 'master-data', component: MasterDataComponent },
	{ path: 'advanced', component: AdvancedComponent },
	{ path: '**', redirectTo: 'master-data' },
];

@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule],
})
export class SettingsRoutingModule {}
