import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { TenantService } from 'src/app/core/services/tenant/tenant.service';

@Component({
	selector: 'app-advanced',
	templateUrl: './advanced.component.html',
	styleUrls: ['./advanced.component.scss'],
})
export class AdvancedComponent {
	constructor(private tenantService: TenantService, private router: Router) {}

	async delete() {
		if (!this.tenantService.currentTenantSubject.value) {
			return;
		}

		if (await this.tenantService.delete(this.tenantService.currentTenantSubject.value)) {
			this.router.navigate(['dashboard']);
		}
	}
}
