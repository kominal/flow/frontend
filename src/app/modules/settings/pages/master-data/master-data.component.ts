import { Component } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { LogService } from '@kominal/lib-angular-logging';
import { currenciesSubject } from 'src/app/core/models/currencies';
import { Tenant } from 'src/app/core/models/tenant';
import { TenantService } from 'src/app/core/services/tenant/tenant.service';

@Component({
	selector: 'app-master-data',
	templateUrl: './master-data.component.html',
	styleUrls: ['./master-data.component.scss'],
})
export class MasterDataComponent {
	public Object = Object;
	public currenciesSubject = currenciesSubject;

	public loading = false;
	public form: FormGroup;

	constructor(public tenantService: TenantService, private logService: LogService) {
		this.form = this.createFormGroup();
		this.tenantService.currentTenantSubject.subscribe((tenant) => {
			this.form = this.createFormGroup(tenant);
		});
	}

	createFormGroup(tenant?: Tenant): FormGroup {
		return new FormGroup({
			_id: new FormControl(tenant?._id),
			name: new FormControl(tenant?.name, Validators.required),
			currency: new FormControl(tenant?.currency, Validators.required),
		});
	}

	async onSubmit() {
		this.loading = true;
		try {
			await this.tenantService.update(this.form.value);
			await this.tenantService.refreshTenants();
		} catch (e) {
			this.logService.handleError(e);
		}
		this.loading = false;
	}
}
