import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { TenantService } from 'src/app/core/services/tenant/tenant.service';

@Component({
	selector: 'app-settings',
	templateUrl: './settings.component.html',
	styleUrls: ['./settings.component.scss'],
})
export class SettingsComponent {
	constructor(public tenantService: TenantService, activatedRoute: ActivatedRoute) {
		activatedRoute.params.subscribe((params) => {
			tenantService.currentTenantIdSubject.next(params.tenantId);
		});
	}
}
