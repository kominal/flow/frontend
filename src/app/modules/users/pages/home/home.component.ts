import { AfterViewInit, Component, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { User, UserService } from '@kominal/core-user-service-angular-client';
import { ConfirmDeleteDialogComponent } from '@kominal/lib-angular-dialog';
import { EMPTY_PAGINATION_RESPONSE, PaginationRequest, PaginationResponse } from '@kominal/lib-angular-pagination';
import { TableComponent } from '@kominal/lib-angular-table';
import { Subject } from 'rxjs';
import { TenantService } from 'src/app/core/services/tenant/tenant.service';
import { AddUserDialogComponent } from '../../components/add-user-dialog/add-user-dialog.component';
import { PermissionsDialogComponent } from '../../components/permissions-dialog/permissions-dialog.component';

@Component({
	selector: 'app-home',
	templateUrl: './home.component.html',
	styleUrls: ['./home.component.scss'],
})
export class HomeComponent implements AfterViewInit {
	@ViewChild(TableComponent, { static: true }) table!: TableComponent;

	displayedColumns: string[] = ['email', 'actions'];

	data = new Subject<PaginationResponse<User>>();

	constructor(private tenantService: TenantService, private matDialog: MatDialog, private userService: UserService) {}

	ngAfterViewInit() {
		this.tenantService.currentTenantSubject.subscribe(() => {
			this.table.requestUpdate();
		});
	}

	async updateData(paginationRequest: PaginationRequest): Promise<void> {
		if (this.tenantService.currentTenantSubject.value) {
			const response = await this.userService.getUsers(paginationRequest);
			this.data.next(response);
		} else {
			this.data.next(EMPTY_PAGINATION_RESPONSE);
		}
	}

	async openPermissionsDialog(user: User) {
		if (!user._id) {
			return;
		}

		const permissions = await this.matDialog
			.open(PermissionsDialogComponent, {
				data: await this.userService.listPermissions(user._id),
			})
			.afterClosed()
			.toPromise();

		if (permissions && user._id) {
			await this.userService.setPermissions(user._id, permissions);
			if (user._id === this.userService.tokenSubject.value?.userId) {
				console.log('Refreshing');
				await this.userService.refresh();
			}
		}
	}

	async removeFromTenant(user: User) {
		const confirmed = await this.matDialog
			.open(ConfirmDeleteDialogComponent, {
				data: user.email,
			})
			.afterClosed()
			.toPromise();

		if (confirmed && user._id) {
			await this.userService.deleteUser(this.tenantService.getCurrentTenantId(), user._id);
			this.table.requestUpdate();
		}
	}

	async addToTenant() {
		const email = await this.matDialog.open(AddUserDialogComponent).afterClosed().toPromise();

		if (email) {
			await this.userService.setPermissions((await this.userService.findUserByEmail(email))._id, []);
			this.table.requestUpdate();
		}
	}
}
