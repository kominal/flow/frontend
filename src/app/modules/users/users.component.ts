import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { TenantService } from 'src/app/core/services/tenant/tenant.service';

@Component({
	selector: 'app-users',
	templateUrl: './users.component.html',
	styleUrls: ['./users.component.scss'],
})
export class UsersComponent {
	constructor(activatedRoute: ActivatedRoute, tenantService: TenantService) {
		activatedRoute.params.subscribe((params) => {
			tenantService.currentTenantIdSubject.next(params.tenantId);
		});
	}
}
