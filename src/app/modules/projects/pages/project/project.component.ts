import { Component } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { LogService } from '@kominal/lib-angular-logging';
import { Project } from 'src/app/core/models/project';
import { ProjectService } from 'src/app/core/services/project/project.service';
import { TenantService } from 'src/app/core/services/tenant/tenant.service';

@Component({
	selector: 'app-project',
	templateUrl: './project.component.html',
	styleUrls: ['./project.component.scss'],
})
export class ProjectComponent {
	public loading = true;
	public form: FormGroup;

	constructor(
		public tenantService: TenantService,
		private projectService: ProjectService,
		private logService: LogService,
		activatedRoute: ActivatedRoute
	) {
		this.form = this.createFormGroup();

		activatedRoute.params.subscribe(async (params) => {
			if (params.projectId) {
				const project = await projectService.get(params.projectId);
				if (project) {
					this.form = this.createFormGroup(project);
				} else {
					this.navigateToParent();
				}
			}
			this.loading = false;
		});
	}

	createFormGroup(project?: Project): FormGroup {
		return new FormGroup({
			_id: new FormControl(project?._id),
			name: new FormControl(project?.name, Validators.required),
		});
	}

	async onSubmit() {
		this.loading = true;
		try {
			await this.projectService.createOrUpdate(this.form.value);
			this.navigateToParent();
		} catch (e) {
			this.logService.handleError(e);
			this.loading = false;
		}
	}

	navigateToParent() {
		this.tenantService.navigateToTenant(['participants']);
	}
}
