import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MaterialModule } from 'src/app/shared/material.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { HomeComponent } from './pages/home/home.component';
import { ProjectsRoutingModule } from './projects-routing.module';
import { ProjectsComponent } from './projects.component';
import { ProjectComponent } from './pages/project/project.component';

@NgModule({
	declarations: [ProjectsComponent, HomeComponent, ProjectComponent],
	imports: [CommonModule, SharedModule, MaterialModule, ProjectsRoutingModule],
})
export class ProjectsModule {}
