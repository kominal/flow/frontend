import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './pages/home/home.component';
import { ImportComponent } from './pages/import/import.component';
import { ImportsComponent } from './pages/imports/imports.component';
import { TransactionComponent } from './pages/transaction/transaction.component';

const routes: Routes = [
	{ path: '', component: HomeComponent },
	{ path: 'imports', component: ImportsComponent },
	{ path: 'imports/create', component: ImportComponent },
	{ path: 'create', component: TransactionComponent },
	{ path: ':transactionId', component: TransactionComponent },
	{ path: '**', redirectTo: '' },
];

@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule],
})
export class TransactionsRoutingModule {}
