import { Component } from '@angular/core';
import { TenantService } from 'src/app/core/services/tenant/tenant.service';

@Component({
	selector: 'app-imports',
	templateUrl: './imports.component.html',
	styleUrls: ['./imports.component.scss'],
})
export class ImportsComponent {
	constructor(public tenantService: TenantService) {}
}
