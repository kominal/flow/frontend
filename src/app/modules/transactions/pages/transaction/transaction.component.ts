import { Component } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { LogService } from '@kominal/lib-angular-logging';
import { BehaviorSubject } from 'rxjs';
import { Account } from 'src/app/core/models/account';
import { Category } from 'src/app/core/models/category';
import { currenciesSubject } from 'src/app/core/models/currencies';
import { Project } from 'src/app/core/models/project';
import { AccountService } from 'src/app/core/services/account/account.service';
import { CategoryService } from 'src/app/core/services/category/category.service';
import { ProjectService } from 'src/app/core/services/project/project.service';
import { TenantService } from 'src/app/core/services/tenant/tenant.service';
import { TransactionService } from 'src/app/core/services/transaction/transaction.service';

@Component({
	selector: 'app-transaction',
	templateUrl: './transaction.component.html',
	styleUrls: ['./transaction.component.scss'],
})
export class TransactionComponent {
	public Object = Object;
	public currenciesSubject = currenciesSubject;

	public loading = true;
	public form: FormGroup;

	public accountsSubject = new BehaviorSubject<Account[]>([]);
	public categoriesSubject = new BehaviorSubject<Category[]>([]);
	public projectsSubject = new BehaviorSubject<Project[]>([]);

	constructor(
		public tenantService: TenantService,
		private transactionService: TransactionService,
		private logService: LogService,
		accountService: AccountService,
		categoryService: CategoryService,
		projectService: ProjectService,
		activatedRoute: ActivatedRoute
	) {
		this.form = new FormGroup({
			_id: new FormControl(),
			debtorAccountId: new FormControl(undefined, Validators.required),
			creditorAccountId: new FormControl(undefined, Validators.required),
			time: new FormControl(undefined, Validators.required),
			amount: new FormControl(undefined, Validators.required),
			currency: new FormControl(undefined, Validators.required),
			categoryIds: new FormControl([]),
			projectIds: new FormControl([]),
			invoiceIds: new FormControl([]),
			purpose: new FormControl(undefined),
		});

		activatedRoute.params.subscribe(async (params) => {
			if (params.transactionId) {
				const transaction = await transactionService.get(params.transactionId);
				if (transaction) {
					transaction.categoryIds = transaction.categoryIds || [];
					transaction.projectIds = transaction.projectIds || [];
					transaction.invoiceIds = transaction.invoiceIds || [];
					this.form.patchValue(transaction);
				} else {
					this.navigateToParent();
				}
			}
			this.loading = false;
		});

		accountService.list({}).then((page) => this.accountsSubject.next(page.items));
		categoryService.list({}).then((page) => this.categoriesSubject.next(page.items));
		projectService.list({}).then((page) => this.projectsSubject.next(page.items));
	}

	async onSubmit() {
		this.loading = true;
		try {
			await this.transactionService.createOrUpdate(this.form.value);
			this.navigateToParent();
		} catch (e) {
			this.logService.handleError(e);
			this.loading = false;
		}
	}

	navigateToParent() {
		this.tenantService.navigateToTenant(['transactions']);
	}
}
