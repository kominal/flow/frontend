import { Component } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { UserService } from '@kominal/core-user-service-angular-client';
import { LogService } from '@kominal/lib-angular-logging';
import { BehaviorSubject } from 'rxjs';
import { accountTypesSubject } from 'src/app/core/models/account';
import { banksSubject } from 'src/app/core/models/banks';
import { Participant } from 'src/app/core/models/participant';
import { AccountService } from 'src/app/core/services/account/account.service';
import { ParticipantService } from 'src/app/core/services/participant/participant.service';
import { TenantService } from 'src/app/core/services/tenant/tenant.service';

@Component({
	selector: 'app-account',
	templateUrl: './account.component.html',
	styleUrls: ['./account.component.scss'],
})
export class AccountComponent {
	public Object = Object;
	public accountTypesSubject = accountTypesSubject;
	public banksSubject = banksSubject;

	public loading = true;
	public form: FormGroup;

	public participantsSubject = new BehaviorSubject<Participant[]>([]);

	constructor(
		public tenantService: TenantService,
		public userService: UserService,
		private accountService: AccountService,
		private logService: LogService,
		participantService: ParticipantService,
		activatedRoute: ActivatedRoute
	) {
		this.form = new FormGroup({
			_id: new FormControl(),
			ownerId: new FormControl(),
			name: new FormControl(undefined, Validators.required),
			participantId: new FormControl(),
			internal: new FormControl(),
			autoImport: new FormControl(),
			type: new FormControl(undefined, Validators.required),
			bank: new FormControl(),
			iban: new FormControl(),
			username: new FormControl(),
			password: new FormControl(),
			email: new FormControl(),
		});
		this.form.controls.internal.valueChanges.subscribe(async (type) => this.updateInternalValidators(type));
		this.form.controls.type.valueChanges.subscribe(async (type) => this.updateTypeValidators(type));
		this.updateInternalValidators();
		this.updateTypeValidators();

		activatedRoute.params.subscribe(async (params) => {
			if (params.accountId) {
				const account = await accountService.get(params.accountId);
				if (account) {
					this.form.patchValue(account);
				} else {
					this.navigateToParent();
				}
			}
			this.loading = false;
		});

		participantService.list({}).then((page) => this.participantsSubject.next(page.items));
	}

	updateInternalValidators(internal?: boolean) {
		this.form.controls.participantId.clearValidators();

		if (!internal) {
			this.form.controls.participantId.setValidators(Validators.required);
		}

		this.form.controls.participantId.updateValueAndValidity();
	}

	updateTypeValidators(type?: string) {
		this.form.controls.bank.clearValidators();
		this.form.controls.iban.clearValidators();
		this.form.controls.email.clearValidators();

		if (type === 'BANK') {
			this.form.controls.bank.setValidators(Validators.required);
			this.form.controls.iban.setValidators(Validators.required);
		} else if (type === 'PAYPAL') {
			this.form.controls.email.setValidators([Validators.required, Validators.email, Validators.minLength(55)]);
		}

		this.form.controls.bank.updateValueAndValidity();
		this.form.controls.iban.updateValueAndValidity();
		this.form.controls.email.updateValueAndValidity();
	}

	async onSubmit() {
		this.loading = true;
		try {
			const value = this.form.value;
			if (value.internal) {
				value.participantId = null;
			}
			await this.accountService.createOrUpdate(value);
			this.navigateToParent();
		} catch (e) {
			this.logService.handleError(e);
			this.loading = false;
		}
	}

	navigateToParent() {
		this.tenantService.navigateToTenant(['accounts']);
	}
}
