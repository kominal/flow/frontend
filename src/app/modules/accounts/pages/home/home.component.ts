import { AfterViewInit, Component, ViewChild } from '@angular/core';
import { PaginationRequest } from '@kominal/lib-angular-pagination';
import { TableComponent } from '@kominal/lib-angular-table';
import { Account } from 'src/app/core/models/account';
import { AccountService } from 'src/app/core/services/account/account.service';
import { TenantService } from 'src/app/core/services/tenant/tenant.service';
@Component({
	selector: 'app-home',
	templateUrl: './home.component.html',
	styleUrls: ['./home.component.scss'],
})
export class HomeComponent implements AfterViewInit {
	@ViewChild(TableComponent) table!: TableComponent<Account>;

	autoColumns = ['name'];
	displayedColumns = ['name', 'type', 'properties', 'actions'];

	constructor(public tenantService: TenantService, private accountService: AccountService) {}

	ngAfterViewInit() {
		this.tenantService.currentTenantIdSubject.subscribe(() => {
			this.table.requestUpdate();
		});
	}

	async updateData(paginationRequest: PaginationRequest): Promise<void> {
		this.table.data.next(await this.accountService.list(paginationRequest));
	}

	async delete(account: Account) {
		if (await this.accountService.delete(account)) {
			this.table.requestUpdate();
		}
	}
}
