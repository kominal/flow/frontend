import { Component } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { LogService } from '@kominal/lib-angular-logging';
import { Estimate } from 'src/app/core/models/estimate';
import { EstimateService } from 'src/app/core/services/estimate/estimate.service';
import { TenantService } from 'src/app/core/services/tenant/tenant.service';

@Component({
	selector: 'app-estimate',
	templateUrl: './estimate.component.html',
	styleUrls: ['./estimate.component.scss'],
})
export class EstimateComponent {
	public loading = true;
	public form: FormGroup;

	constructor(
		public tenantService: TenantService,
		private estimateService: EstimateService,
		private logService: LogService,
		activatedRoute: ActivatedRoute
	) {
		this.form = this.createFormGroup();

		activatedRoute.params.subscribe(async (params) => {
			if (params.estimateId) {
				const category = await estimateService.get(params.estimateId);
				if (category) {
					this.form = this.createFormGroup(category);
				} else {
					this.navigateToParent();
				}
			}
			this.loading = false;
		});
	}

	createFormGroup(estimate?: Estimate): FormGroup {
		return new FormGroup({
			_id: new FormControl(estimate?._id),
		});
	}

	async onSubmit() {
		this.loading = true;
		try {
			await this.estimateService.createOrUpdate(this.form.value);
			this.navigateToParent();
		} catch (e) {
			this.logService.handleError(e);
			this.loading = false;
		}
	}

	navigateToParent() {
		this.tenantService.navigateToTenant(['estimates']);
	}
}
