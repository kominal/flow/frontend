import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { TenantService } from 'src/app/core/services/tenant/tenant.service';

@Component({
	selector: 'app-estimates',
	templateUrl: './estimates.component.html',
	styleUrls: ['./estimates.component.scss'],
})
export class EstimatesComponent {
	constructor(activatedRoute: ActivatedRoute, tenantService: TenantService) {
		activatedRoute.params.subscribe((params) => {
			tenantService.currentTenantIdSubject.next(params.tenantId);
		});
	}
}
