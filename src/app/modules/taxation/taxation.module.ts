import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MaterialModule } from 'src/app/shared/material.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { HomeComponent } from './pages/home/home.component';
import { TaxationRoutingModule } from './taxation-routing.module';
import { TaxationComponent } from './taxation.component';

@NgModule({
	declarations: [TaxationComponent, HomeComponent],
	imports: [CommonModule, SharedModule, MaterialModule, TaxationRoutingModule],
})
export class TaxationModule {}
