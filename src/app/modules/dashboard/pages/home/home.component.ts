import { Component } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { Dashboard } from 'src/app/core/models/dashboard';
import { DashboardService } from 'src/app/core/services/dashboard/dashboard.service';
import { TenantService } from 'src/app/core/services/tenant/tenant.service';

@Component({
	selector: 'app-home',
	templateUrl: './home.component.html',
	styleUrls: ['./home.component.scss'],
})
export class HomeComponent {
	dashboardSubject = new BehaviorSubject<Dashboard | undefined>(undefined);

	constructor(dashboardService: DashboardService, tenantService: TenantService) {
		if (tenantService.currentTenantIdSubject.value) {
			dashboardService.get().then((dashboard) => this.dashboardSubject.next(dashboard));
		}
	}
}
