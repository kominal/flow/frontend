import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MaterialModule } from 'src/app/shared/material.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { HomeComponent } from './pages/home/home.component';
import { ReportingRoutingModule } from './reporting-routing.module';
import { ReportingComponent } from './reporting.component';

@NgModule({
	declarations: [ReportingComponent, HomeComponent],
	imports: [CommonModule, SharedModule, MaterialModule, ReportingRoutingModule],
})
export class ReportingModule {}
