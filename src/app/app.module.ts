import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AuthenticationInterceptor, CoreUserServiceModule } from '@kominal/core-user-service-angular-client';
import { PapyrusClientModule } from '@kominal/papyrus-angular-client';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './core/components/header/header.component';
import { MaterialModule } from './shared/material.module';

@NgModule({
	declarations: [AppComponent, HeaderComponent],
	imports: [
		BrowserModule,
		AppRoutingModule,
		HttpClientModule,
		MaterialModule,
		PapyrusClientModule.forRoot({ projectName: 'flow', languages: ['de', 'en'] }),
		CoreUserServiceModule.forRoot({
			loginRedirectUrl: '/dashboard',
			changePasswordRedirectUrl: '/dashboard',
		}),
		BrowserAnimationsModule,
	],
	providers: [
		{
			provide: HTTP_INTERCEPTORS,
			useClass: AuthenticationInterceptor,
			multi: true,
		},
	],
	bootstrap: [AppComponent],
})
export class AppModule {}
